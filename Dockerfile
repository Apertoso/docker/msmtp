FROM debian:sid

MAINTAINER Accomodata <support@accomodata.be>

RUN set -e \
    # Update APT cache & install upgrades
    && apt-get update \
    && apt-get upgrade -y \
    \
    # Install HTTPS APT & curl transport
    && apt-get install -y --no-install-recommends \
        msmtp \
        msmtp-mta \
        ca-certificates \
    && echo OK

EXPOSE 25