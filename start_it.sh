#!/usr/bin/env bash

## EDIT these variables below to fit the instance config
CERTFILE="erp.accomodata.be"
RELAYHOST="o365-accomodata-net.mail.protection.outlook.com"
SLUGNAME="accomodata"


cat > msmtp-config << EOF
defaults
port 25
tls on

logfile -

tls_trust_file /etc/ssl/certs/ca-certificates.crt

account default
host ${RELAYHOST}
tls_cert_file /etc/ssl/private/${CERTFILE}.crt
tls_key_file /etc/ssl/private/${CERTFILE}.key
EOF

docker run \
    --detach \
    --name=msmtp_${SLUGNAME} \
    --network=docker_backend \
    --label=restart=weekly \
    --volume=${PWD}/msmtp-config:/root/.msmtprc \
    --volume=/srv/acme-lego/certificates:/etc/ssl/private \
    registry.gitlab.com/apertoso/docker/msmtp \
    msmtpd --interface=0.0.0.0

